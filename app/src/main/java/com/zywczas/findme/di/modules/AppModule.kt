package com.zywczas.findme.di.modules

import com.zywczas.findme.SessionManager
import com.zywczas.findme.SessionManagerImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class AppModule {



}