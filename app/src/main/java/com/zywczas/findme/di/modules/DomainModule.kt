package com.zywczas.findme.di.modules

import com.zywczas.findme.SessionManager
import com.zywczas.findme.SessionManagerImpl
import com.zywczas.findme.loginfragment.domain.LoginRepository
import com.zywczas.findme.loginfragment.domain.LoginRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped
import javax.inject.Singleton

@Module
@InstallIn(ActivityRetainedComponent::class)
abstract class DomainModule {

    @Binds
    @Singleton
    abstract fun provideSessionManager(manager: SessionManagerImpl): SessionManager

    @Binds
    @ActivityRetainedScoped
    abstract fun bindLoginRepository(repo: LoginRepositoryImpl) : LoginRepository

}