package com.zywczas.findme.mainactivity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.zywczas.findme.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

//    @Inject
//    lateinit var fragmentFactory : UniversalFragmentFactory

    override fun onCreate(savedInstanceState: Bundle?) {
//        supportFragmentManager.fragmentFactory = fragmentFactory
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }


}