package com.zywczas.findme

interface SessionManager {

    suspend fun isNetworkAvailable() : Boolean

    suspend fun isUserLoggedIn() : Boolean

}